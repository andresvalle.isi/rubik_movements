#         1 2 3
#         4 5 6
#         7 8 9
# 
# 1 2 3   1 2 3   1 2 3   1 2 3 
# 4 5 6   4 5 6   4 5 6   4 5 6
# 7 8 9   7 8 9   7 8 9   7 8 9
# 
#         1 2 3
#         4 5 6
#         7 8 9

class Cube
    def new
        self.setup_faces
        self.setup_movements
    end

    def setup_faces
        @faces = {}
        (1..6).each do |number|
            face = Face.new( number )
            @faces[ number ] = face
        end
    end

    def setup_movements
        @movements = { #%q{ R R' L L' F F' T T' B B' }
            "R": 'right',
            "R'": 'right!',
            "L": 'left',
            "L'": 'left!',
            "F": 'front',
            "F'": 'front!',
            "T": 'top',
            "T'": 'top!',
            "B": 'bottom',
            "B'": 'bottom!'
        }
    end

    def scramble
        (1..100).each do |x|
            self.random_movement
        end
    end

    def do_movement(movement)
        movement_method = @movements[ movement ]
        self.movement_method
    end

    def random_movement
        movement = (1..9).random # FIXME
        self.do_movement(movement)
    end
        
    def right
        aux = @faces[1][3]
    end

    def debug
        # TODO
    end

end

class Face
    def new(face_id)
        @face_id = face_id
        @mini_cubes = (1.9).map{ |n| n }
    end
end

class Color
    def new(color_id, letter)
        @color_id = color_id
        @letter = letter
    end
end

rubik = Cube.new()
rubik.scramble()

puts rubik.debug()